import os
import sys
import logging
import random
from types import IntType 


logformatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

def create_log(name, file=None):
    if not file:
        file = name
        name = str(random.randrange(1, 99999))
    log = logging.getLogger(name)
    handler = logging.FileHandler( file )
    handler.setFormatter( logformatter )
    log.addHandler( handler )
    log.setLevel( logging.INFO )
    return log
    
    
def get_pid(pid):
    if type(pid) == IntType:
        return pid
    try:
        f = open(pid,'r')
        pid_int = int(f.read().split()[0])
        f.close()
        return pid_int
    except IOError:
        return
    except ValueError:
        return
    except IndexError:
        return
 
def is_live(pid):
    pid = get_pid(pid)
    if not pid:
        return
    try:
        os.kill(pid, 0)
    except OSError:
        return

def make_pid(filename):
    pid = is_live(filename)
    if pid:
        print('My PID {pid} already exists.'.format(pid=pid))
        sys.exit()
    pid = os.getpid()
    f = open(filename,'w')
    f.write(str(pid))
    f.close()
    return pid
