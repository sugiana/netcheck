pid_file = 'netcheck.pid'
log_file = 'netcheck.log'
state_dir = ''
url = 'https://127.0.0.1:6543/rpc'
username = ''
password = ''
interval = None # exit after first execution 
#interval = 10 # execute every n seconds
hosts = {
    'default-gateway': {
        'ip': '192.168.1.1',
        'description': 'IndiHome Modem',
        },
    'database': {
        'ip': '10.8.20.1',
        'port': 5432,
        'description': 'Primary Host - PostgreSQL',
        'gateway': 'default-gateway',
        },
    }
