import sys
import os
import subprocess
import xmlrpclib
import socket
socket.setdefaulttimeout(30)
import imp
from time import sleep 
from optparse import OptionParser
from tools import (
    make_pid,
    create_log,
    )


CONNECTED = 0
DISCONNECTED = -1
REFUSE = -2

CONN_DESC = {
    CONNECTED: 'connected',
    DISCONNECTED: 'disconnected',
    REFUSE: 'refuse',
    }

def ping(name):
    profile = conf.hosts[name]
    ip = profile['ip']
    if 'port' in profile:
        port = profile['port']
        connected = telnet(ip, port)
        print('DEBUG: {n} telnet {ip} {port} {c}'.format(n=name, ip=ip, port=port,
            c=connected))
        if connected:
            set_last_state(name, CONNECTED)
            return connected
    c1 = ['ping', '-c1', '-w', '2', ip]
    c2 = ['grep', 'received']
    c3 = ['cut', '-f4', '-d', ' ']
    p1 = subprocess.Popen(c1, stdout=subprocess.PIPE)
    p2 = subprocess.Popen(c2, stdin=p1.stdout, stdout=subprocess.PIPE)
    p3 = subprocess.Popen(c3, stdin=p2.stdout, stdout=subprocess.PIPE)
    s = p3.communicate()[0].strip()
    connected = s == '1'
    print('DEBUG: {n} ping {ip} {c}'.format(n=name, ip=ip, c=connected))
    if connected:
        if 'port' in profile:
            set_last_state(name, REFUSE)
        else:
            set_last_state(name, CONNECTED)
    else:
        set_last_state(name, DISCONNECTED)
    return connected

def telnet(ip, port):
    code = sock.connect_ex((ip, port))
    return code in (0, 106)

def ping_all(name):
    if ping(name):
        set_gateway_connected(name)
        return True
    profile = conf.hosts[name]
    if 'gateway' in profile:
        return ping_all(profile['gateway'])

def set_gateway_connected(name):
    profile = conf.hosts[name]
    if 'gateway' not in profile:
        return
    name = profile['gateway']
    profile = conf.hosts[name]
    p = {'description': profile['description'],
         'ip': profile['ip'],
         'status_id': CONNECTED}
    send_last_state(p)
    set_gateway_connected(name)

def file_state(name):
    filename = 'netcheck-{n}.info'.format(n=name)
    return os.path.join(conf.state_dir, filename)

def get_last_state(name):
    filename = file_state(name)
    if os.path.exists(filename):
        return int(open(filename).read())

def set_last_state(name, state_id):
    last_state = get_last_state(name)
    filename = file_state(name)
    f = open(filename, 'w')
    f.write(str(state_id))
    f.close()
    if last_state == state_id:
        print('Last state masih sama, abaikan.')
        return
    profile = conf.hosts[name]
    ip = profile['ip']
    p = {'description': profile['description'],
         'ip': ip,
         'status_id': state_id,
         }
    c_desc = CONN_DESC[state_id]
    net_tool = 'ping'
    if state_id == CONNECTED:
        if 'port' in profile:
            net_tool = 'telnet'
        f = log_info
    else:
        f = log_error
        if state_id == REFUSE:
            net_tool = 'telnet'
    if net_tool == 'telnet':
        ip += ' ' + str(profile['port'])
    f('{n} {t} {ip} {c}'.format(n=name, t=net_tool, ip=ip, c=c_desc))
    if 'port' in profile:
        p['port'] = profile['port']
    send_last_state(p)
    if 'event' in profile:
        p['event'] = profile['event']
        run_external_scripts(p)

def send_last_state(p):
    if conf.username:
        p['username'] = conf.username
        p['password'] = conf.password
        send_last_state_(p)

def send_last_state_(p):
    p_ = dict(p)
    p_['password'] = '...'
    log_info('Send {url} {p}'.format(url=conf.url, p=p_))
    resp = rpc.net_status(p)
    log_info('Recv {r}'.format(r=resp))

def run_external_scripts(p):
    desc = CONN_DESC[p['status_id']]
    if desc not in p['event']:
        return
    script = p['event'][desc]
    subprocess.call(script)

def get_first_hosts():
    gateways = []
    for name in conf.hosts:
        profile = conf.hosts[name]
        if 'gateway' in profile:
            gateway = profile['gateway']
            if gateway not in gateways:
                gateways.append(gateway)
    r = []
    for name in conf.hosts:
        if name not in gateways:
            r.append(name)
    return r

def log_info(s):
    print(s)
    log.info(s)

def log_error(s):
    print(s)
    log.error(s)


conf_file = 'conf.py'
pars = OptionParser()
pars.add_option('-c', '--conf', default=conf_file,
        help='Configuration file, default: ' + conf_file)
option, remain = pars.parse_args(sys.argv[1:])

conf_file = option.conf
conf = imp.load_source('conf', conf_file)

pid = make_pid(conf.pid_file)
log = create_log(conf.log_file)
rpc = xmlrpclib.ServerProxy(conf.url)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

first_hosts = get_first_hosts()

try:
    while True:
        for name in first_hosts: 
            ping_all(name)
        if not conf.interval:
            break
        sleep(conf.interval)
except KeyboardInterrupt:
    pass

os.remove(conf.pid_file)
